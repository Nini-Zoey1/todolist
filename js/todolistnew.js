$(function() {
    load();
    $("#title").on("keydown", function(event) {
        if (event.keyCode === 13) {
            if ($(this).val() === "") {
                alert("Please input you TO-DO item!");
            } else {
                var local = getCode();
                local.push({ title: $(this).val(), done: false });
                saveData(local);
                load();
            }
        }
    });

    $("ol, ul").on("click", "a", function() {
        var data = getCode();
        var index = $(this).attr("id");
        data.splice(index, 1);
        saveData(data);
        load();
    });

    $("ol, ul").on("click", "input", function() {
        var data = getCode();
        var index = $(this).siblings("a").attr("id");
        data[index].done = $(this).prop("checked");
        saveData(data);
        load();
    });



    function getCode() {
        var data = localStorage.getItem("todolist");
        if (data !== null) {
            return JSON.parse(data);
        } else {
            return [];
        }
    }

    function saveData(data) {
        data = JSON.stringify(data);
        localStorage.setItem("todolist", data);
    }

    function load() {
        var data = getCode();
        $("ol, ul").empty();
        var todoCount = 0;
        var doneCount = 0;
        $.each(data, function(i, n) {
            if (n.done) {
                $("ul").prepend("<li><input type='checkbox' checked='checked' > <p>" + n.title + "</p> <a href='javascript:;' id=" + i + " ></a></li>");
                doneCount++;
            } else {
                $("ol").prepend("<li><input type='checkbox'> <p>" + n.title + "</p> <a href='javascript:;' id=" + i + " ></a></li>");
                todoCount++;
            }
        });
        // console.log(todoCount);
        // console.log(doneCount);
        $("#todocount").text(todoCount);
        $("#donecount").text(doneCount);
    }
})